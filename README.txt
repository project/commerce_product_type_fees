README.txt
------------
Commerce Product Type Fees module for Drupal.

INTRODUCTION
------------
This module helps to manage custom fees per product type. It provides a 
configuration form from where the user can add as many fees in percentage as he
 wants.

When the user will add any product of those product type in there cart, those 
fees will be applied to the order subtotal.

* For a full description of the module, visit the project page:
   https://www.drupal.org/project/commerce_product_type_fees

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/commerce_product_type_fees


REQUIREMENTS
------------
This module requires the following module:
* Drupal Commerce 2.x (https://www.drupal.org/project/commerce)


INSTALLATION
------------
Install the module as usual, more info can be found on:
https://www.drupal.org/docs/extending-drupal/installing-modules


CONFIGURATION
------------
Be sure to enable Commerce Product Type Fees.
o Go to configure fees page:
  admin/commerce/config/commerce_product_type_fees/settings
